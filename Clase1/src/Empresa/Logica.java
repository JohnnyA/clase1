/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Empresa;

/**
 *
 * @author Johnny
 */
public class Logica {

    public void congelarAire(double compNitrogeno, double copmOxigeno, double compDiCarbono, double copmVaAgua) {
        System.out.println("cantidad Nitrogeno: " + compNitrogeno + "%"
                + "\nCantidad Oxigeno:" + copmOxigeno + "%"
                + "\nCantidad dioxido de carbono:" + compDiCarbono + "%"
                + "\nCantidad Vapor de agua:" + copmVaAgua + "%");

    }

    public void congelarAgua(double salinidadAgua) {
        System.out.println("Salinidad de agua: " + salinidadAgua + "gramos por litro de agua");

    }

    public void congelarNitrogeno(int tiempo, String metodoCongelacion) {
        System.out.println("Tiempo exposicion: " + tiempo + "segundos"
                + "\nMetodo congelacion: " + metodoCongelacion);

    }
}
