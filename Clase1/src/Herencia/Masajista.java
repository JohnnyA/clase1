/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Herencia;

/**
 *
 * @author Johnny
 */
public class Masajista extends SeleccionFutbol{

    String titulacion;
    int aniosExperiencia;

    public Masajista(String titulacion, int aniosExperiencia, int id, String nombre, String apellidos, int edad) {
        super(id, nombre, apellidos, edad);
        this.titulacion = titulacion;
        this.aniosExperiencia = aniosExperiencia;
    }

  

    public String getAtributos() {
        return "Titulacion: " + titulacion
                + "\nAños de experiencia: " + aniosExperiencia
                + "\nID: " + id
                + "\nNombre: " + nombre
                + "\nApellidos: " + apellidos
                + "\nEdad: " + edad;

    }

    public void darMasajes() {
        System.out.println("Dar masajes: " + true);
    }
}
