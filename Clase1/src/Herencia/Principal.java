/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Herencia;

/**
 *
 * @author Johnny
 */
public class Principal {

    public static void main(String[] args) {
        SeleccionFutbol sele1 = new SeleccionFutbol(101, "Johnny", "Mendez", 20);

        Futbolista sele2 = new Futbolista(13,"portero", 12, "Fabian", "Ruiz", 21);

        Entrenador sele3 = new Entrenador("12", 2014, "Carlos", "Lopez", 30);

        Masajista sele4 = new Masajista("experto", 10, 1202, "Oscar", "Duran", 40);

        System.out.println(sele1.getAtributos());
        sele1.concentrarse();
        sele1.viajar();
        System.out.println("");
        System.out.println(sele2.getAtributos());
        sele2.entrenar();
        sele2.jugarPartido();
        System.out.println("");
        System.out.println(sele3.getAtributos());
        sele3.dirigirEntrenamineto();
        sele3.dirigirPartido();
        System.out.println("");
        System.out.println(sele4.getAtributos());
        sele4.darMasajes();
    }

}
