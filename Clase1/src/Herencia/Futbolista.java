/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Herencia;

/**
 *
 * @author Johnny
 */
public class Futbolista extends SeleccionFutbol{

    int dorsal;
    String demarcacion;

    public Futbolista(int dorsal, String demarcacion, int id, String nombre, String apellidos, int edad) {
        super(id, nombre, apellidos, edad);
        this.dorsal = dorsal;
        this.demarcacion = demarcacion;
    }

   

    public String getAtributos() {
        return "Dorsal: " + dorsal
                + "\nid: " + demarcacion
                + "\nNombre: " + nombre
                + "\nApellidos: " + apellidos
                + "\nEdad: " + edad;
    }

    public void jugarPartido() {
        System.out.println("Jugar partido: " + true);
    }

    public void entrenar() {
        System.out.println("Entrenar: " + false);

    }

    public int getDorsal() {
        return dorsal;
    }

    public void setDorsal(int dorsal) {
        this.dorsal = dorsal;
    }

    public String getDemarcacion() {
        return demarcacion;
    }

    public void setDemarcacion(String demarcacion) {
        this.demarcacion = demarcacion;
    }

}
