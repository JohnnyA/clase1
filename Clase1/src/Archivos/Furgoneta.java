/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Archivos;

/**
 *
 * @author Johnny
 */
public class Furgoneta extends Vehiculo{

    int capacidad;
    boolean disponible;

    public Furgoneta(int capacidad, boolean disponible, String placa, String marca, String color) {
        super(placa, marca, color);
        this.capacidad = capacidad;
        this.disponible = disponible;
    }

}
