/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Archivos;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import javax.swing.JOptionPane;

/**
 *
 * @author Johnny
 */
public class Logica extends Vehiculo {

    String placaaaa;

    public static String simboloSplit = ";";
    public static String simboloDolar = "$";
    public static String simboloSplitRegistro = "\\;";
    public static String simboloDolarRegistro = "\\$";

    private String[] espacios;

    public Logica(String[] espacios, String placa, String marca, String color) {
        super(placa, marca, color);
        this.espacios = espacios;
    }

    private boolean hayEspacio() {
        boolean disponible = false;
        for (int i = 0; i < this.espacios.length; i++) {
            if (this.espacios[i].equals("")) {
                disponible = true;
            }
        }
        return disponible;
    }

    private void inicializar() {
        for (int i = 0; i < this.espacios.length; i++) {
            this.espacios[i] = "";
        }
    }

    private boolean buscarVocales(String pPlaca) {
        String vocales = "aeiou";
        boolean bandera = false;
        for (int i = 0; i < pPlaca.length(); i++) {
            for (int j = 0; j < vocales.length(); j++) {
                if (pPlaca.charAt(i) == vocales.charAt(j)) {
                    System.out.println("esta placa no esta permitida");
                    return bandera = true;
                }

            }
        }
        return bandera;
    }

    public static void main(String[] args) {
        Logica log = new Logica(args, simboloDolar, simboloDolar, simboloDolar);
        String placa = JOptionPane.showInputDialog("Digite su numero de placa");
        log.buscarVocales(placa);
    }

    private boolean yaEstaLaPlaca(String pPlaca) {

        boolean retorno = false;
        for (int i = 0; i < this.espacios.length; i++) {
            if (this.espacios[i].equals(pPlaca)) {
                retorno = true;

            }

        }
        return retorno;
    }

    public String registrarVehiculo(String pPlaca) {
        String resultado = "";
        if (this.hayEspacio()) {
            if (this.buscarVocales(pPlaca)) {
                if (this.yaEstaLaPlaca(pPlaca)) {
                    resultado = "El vehiculo ya esta registrado";

                } else {
                    boolean alojado = false;
                    for (int i = 0; i < this.espacios.length; i++) {
                        if ((this.espacios[i].equals("")) && (!alojado)) {
                            alojado = true;

                            this.espacios[i] = pPlaca;
                            resultado = " El vehículo se registro en la posición:" + (i + 1);
                        }
                    }
                }

            } else {
                resultado = "La placa no puede contener vocales. ";
            }

        } else {
            resultado = "No se puede registrar";
        }
        return resultado;

    }

    public String getPlaca() {
        return placa;
    }

    public void setPlaca(String placa) {
        this.placa = placa;
    }

    public void ResgistarV(RegistroVehiculos espacios[]) throws IOException {

        String Ruta = "..\\Users\\Johnny\\Documents\\NetBeansProjects\\clase1\\Clase1\\src\\Archivos";

        File archivo = new File(Ruta);
        BufferedWriter bw = null;

        if (archivo.exists()) {
            bw = new BufferedWriter(new FileWriter(archivo));
            for (int i = 0; i < espacios.length; i++) {
                if (espacios[i] != null) {
                    bw.write(simboloDolar + this.espacios[i]);

                }
            }

        } else {
            bw = new BufferedWriter(new FileWriter(archivo));
            for (int i = 0; i < espacios.length; i++) {
                if (espacios[i] != null) {
                    bw.write(simboloDolar);

                }
            }
        }
        bw.close();
    }

    public String getPlacaaaa() {
        return placaaaa;
    }

    public void setPlacaaaa(String placaaaa) {
        this.placaaaa = placaaaa;
    }

    public static String getSimboloSplit() {
        return simboloSplit;
    }

    public static void setSimboloSplit(String simboloSplit) {
        Logica.simboloSplit = simboloSplit;
    }

    public static String getSimboloDolar() {
        return simboloDolar;
    }

    public static void setSimboloDolar(String simboloDolar) {
        Logica.simboloDolar = simboloDolar;
    }

    public static String getSimboloSplitRegistro() {
        return simboloSplitRegistro;
    }

    public static void setSimboloSplitRegistro(String simboloSplitRegistro) {
        Logica.simboloSplitRegistro = simboloSplitRegistro;
    }

    public static String getSimboloDolarRegistro() {
        return simboloDolarRegistro;
    }

    public static void setSimboloDolarRegistro(String simboloDolarRegistro) {
        Logica.simboloDolarRegistro = simboloDolarRegistro;
    }

}
